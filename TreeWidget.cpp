//######
//## TreeWidget.cpp 
//## ==============
//## 18.06.2018: Ré-écriture complète
//######

#include  "TreeWidget.h"

#include "I_ClosedFolder_16.xpm"
#include "I_NormalFile_16.xpm"

#define K_FN  0
#define K_CK  1
#define K_SZ  2
#define K_CS  3  // Checksum
#define K_PN  4
#define K_ZZ  5


//
// <<<< myTreeWidget::myTreeWidget
//
myTreeWidget::myTreeWidget( QWidget * p_Parent ): QTreeWidget( p_Parent )
{
  //-- Add Subdirectories as children  when user clicks on a file item,
  //-- otherwise adding all children recursively may consume HUGE amount of memory

  QTreeWidgetItem * HeaderItem = new QTreeWidgetItem();

  HeaderItem->setText( K_CK, QString( " "  ) );
  HeaderItem->setText( K_FN, QString( "Fichier"   ) );
  HeaderItem->setText( K_SZ, QString( "Taille (octets)") );
  HeaderItem->setText( K_CS, QString( "Checksum" ) );
  HeaderItem->setText( K_PN, QString( "Path" ) );
  HeaderItem->setText( K_ZZ, QString( " "  ) );

  this->setHeaderItem( HeaderItem );

  this->setSortingEnabled( true );
  this->sortItems( 0, Qt::AscendingOrder );

  this->Adjust_ColumnSize();

  connect( this , SIGNAL( itemClicked     ( QTreeWidgetItem *, int ) )
         , this , SLOT  ( slot_itemClicked( QTreeWidgetItem *, int ) ) );

}
// >>>> myTreeWidget::myTreeWidget


//
// <<<< myTreeWidget::~myTreeWidget
//
myTreeWidget::~myTreeWidget()
{
}
// >>>> myTreeWidget::~myTreeWidget


//
// <<<< myTreeWidget::Add_FirstChild
//
void myTreeWidget::Add_FirstChild( QString p_FilePath )
{
  QFileInfo fileInfo( p_FilePath );

  if ( fileInfo.isSymLink() ) {
    return;
  }

  QString FileName = fileInfo.fileName();

  QTreeWidgetItem * child = new QTreeWidgetItem();

  child->setCheckState( K_CK, Qt::Checked );
  child->setTextAlignment( K_CK, Qt::AlignCenter );

  child->setText( K_FN, fileInfo.fileName() );

  child->setIcon( K_FN, *( new QIcon( QPixmap( (const char **) I_ClosedFolder_16 ) ) ) );

  child->setText( K_CS, "" );

  child->setText( K_PN, fileInfo.absoluteFilePath() );

  this->clear();
  this->addTopLevelItem( child );

  this->Add_Children( p_FilePath, child );

  child->setExpanded( true );

  this->Adjust_ColumnSize();
}
// >>>> myTreeWidget::Add_FirstChild


//
// <<<< myTreeWidget::Add_Children
//
void myTreeWidget::Add_Children( QString p_FilePath, QTreeWidgetItem * p_Item )
{
  if ( p_Item && p_Item->childCount() != 0 ) return;

  QDir * DIR = new QDir( p_FilePath );
  QFileInfoList filesList = DIR->entryInfoList();

  foreach ( QFileInfo fileInfo, filesList ) {

    if ( fileInfo.isSymLink() ) continue;

    QString FileName = fileInfo.fileName();

    if ( FileName == "."  ) continue;
    if ( FileName == ".." ) continue;

    QTreeWidgetItem * child = new QTreeWidgetItem();

    child->setTextAlignment( K_CK, Qt::AlignCenter );

    child->setText( K_FN, fileInfo.fileName() );

    if ( fileInfo.isFile() ) {
      child->setIcon( K_FN, *(new QIcon( QPixmap( (const char **) I_NormalFile_16 ) ) ) );
      child->setText( K_SZ, QString::number( fileInfo.size() ) );
      child->setTextAlignment( K_SZ, Qt::AlignRight );
      child->setCheckState( K_CK, Qt::Checked );
    }

    if ( fileInfo.isDir() ) {
      child->setIcon( K_FN, *( new QIcon( QPixmap( (const char **) I_ClosedFolder_16 ) ) ) );
      child->setText( K_PN, fileInfo.absoluteFilePath() );
      child->setCheckState( K_CK, Qt::Checked );
    }

   if ( p_Item == 0 ) {
      this->addTopLevelItem( child );
    } else {
      p_Item->addChild( child );
    }
  }
}
// >>>> myTreeWidget::Add_Children


//
// <<<< myTreeWidget::Adjust_ColumnSize
//
void myTreeWidget::Adjust_ColumnSize()
{
  this->resizeColumnToContents( K_FN );
  this->resizeColumnToContents( K_CK );
  this->resizeColumnToContents( K_SZ );
  this->resizeColumnToContents( K_PN );
  this->resizeColumnToContents( K_ZZ );

  this->update();

  int Size_PN = this->columnWidth( K_PN );
  int Size_ZZ = this->columnWidth( K_ZZ );

  int MaxSize = 1;

  if ( Size_ZZ > MaxSize ) {
    Size_PN = Size_PN + Size_ZZ  - MaxSize;
    Size_ZZ = MaxSize;
  }

  this->setColumnWidth( K_PN, Size_PN );
  this->setColumnWidth( K_ZZ, Size_ZZ );
}
// >>>> myTreeWidget::Adjust_ColumnSize


//
// <<<< myTreeWidget::Get_PathName
//
QString myTreeWidget::Get_PathName( QTreeWidgetItem* p_Item )
{
  if ( ! p_Item ) {
    return "";
  }

  QString FileName = p_Item->text( K_FN );
  QString PathName = p_Item->text( K_PN );

  if ( FileName.isEmpty() ) {
    return "";
  }

  if ( PathName.isEmpty() ) {
    QTreeWidgetItem * p_Parent = p_Item->parent();

    PathName = p_Parent->text( K_PN ) + QString( "/" ) + FileName;
  }

  return PathName;
}
// >>>> myTreeWidget::Get_PathName


//
// <<<< myTreeWidget::Calculer_Checksum
//
void myTreeWidget::Calculer_Checksum( QString p_FilePath )
{
  QMessageBox MsgBox;

  MsgBox.setIcon( QMessageBox::Warning );

  MsgBox.setWindowTitle( "Calculer_Checksum>" );
  MsgBox.setText( "Calculer_Checksum...." +  p_FilePath );
  MsgBox.setStandardButtons( QMessageBox::Ok );
  MsgBox.setDefaultButton( QMessageBox::Ok );

  (void) MsgBox.exec();
}
// >>>> myTreeWidget::Calculer_Checksum


//
// <<<< myTreeWidget::keyPressEvent
//
void myTreeWidget::keyPressEvent( QKeyEvent * p_Event )
{
  bool b_IGNORE = false;

  QTreeWidgetItem * qItem = this->currentItem();

  switch ( p_Event->key() ) {
  case Qt::Key_Space:

   if ( qItem ) {
      bool b_WasChecked = ( qItem->checkState( K_CK ) == Qt::Checked );

      if ( b_WasChecked ) {
        qItem->setCheckState( K_CK, Qt::Unchecked );
      } else {
        qItem->setCheckState( K_CK, Qt::Checked );
      }

    }

    b_IGNORE = true;

    break;

  case Qt::Key_F5:
    this->Adjust_ColumnSize();
    break;

  default:
    break;
  }

  if ( b_IGNORE ) {
    p_Event->ignore();
  } else {
    QTreeView::keyPressEvent( p_Event );
  }
}
// >>>> myTreeWidget::keyPressEvent


//
// <<<< myTreeWidget::mousePressEvent
//
void myTreeWidget::mousePressEvent( QMouseEvent* p_Event )
{
  QPoint            qPoint;
  QTreeWidgetItem * qItem;

  if ( p_Event->button() == Qt::LeftButton ) {

    qPoint = QPoint( p_Event->x(), p_Event->y() );

    qItem  = (QTreeWidgetItem * ) this->itemAt( qPoint );

    int k_Column;

    if ( qItem ) {
      k_Column = this->columnAt( qPoint.x() );

      if ( k_Column > 0 ) {
        //p_Event->accept();
      }

      if ( k_Column == K_CK ) {

        bool b_IsUnchecked = ( qItem->checkState( K_CK ) == Qt::Unchecked );

        if ( b_IsUnchecked ) {
          qDebug() << "Sélectionné";
        }
      }  

      if ( k_Column == K_CK ) {

        bool b_IsChecked = ( qItem->checkState( K_CK ) == Qt::Checked );

        if ( b_IsChecked ) {
          qDebug() << "Non-Sélectionné";
        }
      }

    } 
  } 

  QTreeWidget::mousePressEvent( p_Event );
}
// >>>> myTreeWidget::mousePressEvent


//
// <<<< myTreeWidget::contextMenuEvent
//
void myTreeWidget::contextMenuEvent( QContextMenuEvent * p_Event )
{
  QMenu * PopupM = new QMenu(); // = this->createStandardContextMenu();

  QAction * m_ACTION;

  m_ACTION = PopupM->addAction( "Calculer Chechsum"
           , this, SLOT( slot_Calculer_Checksum() ) );

  QPoint PM_Point = QCursor::pos() + QPoint( 16, 16 );

  m_ACTION = PopupM->exec( PM_Point );
}
// >>>> myTreeWidget::contextMenuEvent


//
// <<<< myTreeWidget::slot_itemClicked
//
void myTreeWidget::slot_itemClicked( QTreeWidgetItem* p_Item, int )
{
  QString FilePath = p_Item->text( K_PN );

  if ( FilePath.isEmpty() ) return;

  QFileInfo FI_Path( FilePath );

  if ( FI_Path.isSymLink() ) return;
  if ( FI_Path.isFile()    ) return;

  this->Add_Children( FilePath, p_Item );

  this->Adjust_ColumnSize();
}
// >>>> myTreeWidget::slot_itemClicked


//
// <<<< myTreeWidget::slot_Calculer_Checksum
//
void myTreeWidget::slot_Calculer_Checksum()
{
  Calculer_Checksum( "" );
}
// >>>> myTreeWidget::slot_Calculer_Checksum



