//######
//## Mainwindow.h
//## ============
//## 18.06.2018: Ré-écriture complète
//######
    
#ifndef MAINWINDOW_H
#define MAINWINDOW_H
    
#include "TreeWidget.h"
    
#include <QtCore>
#include <QtWidgets>

#define  ERR_WRITING_FILE   QString( "Erreur d'Ecriture Fichier : %1" )
#define  ERR_READING_FILE   QString( "Erreur de Lecture Fichier : %1" )
#define  ERR_ATTRIBUTE_FILE QString( "Erreur Attributs du Fichier : %1" )
#define  ERR_OPEN_READONLY  QString( "Erreur Ouverture Fichier (ReadOnly)  : %1" )
#define  ERR_OPEN_WRITEONLY QString( "Erreur Ouverture Fichier (WriteOnly) : %1" )
    
class myTreeWidget;
    
class myMainWindow: public QMainWindow
{   
    Q_OBJECT
    
public:
    myMainWindow( QWidget * p_Parent = 0 );
    
    void Box_Information( const QString &p_Titre, const QString &p_Msg );

    QString Get_Tab_PathName( int p_Index );

    void Ouvrir_Dossier( QString p_PathDossier );
    void Sauver_Cartographie( QString p_PathDossier );

    void Statut_Fichier( QString p_Path );
    
public slots:
    void slot_Ouvrir_Dossier();
    void slot_Sauver_Cartographie();
    void slot_Quitter();

    void slot_APropos();
    
    void slot_Statut_Fichier( QTreeWidgetItem *p_Item, int p_Column  );
    
protected:
    QString           _Path_Dossier;
    
    QSplitter        *_Splitter;
    
    myTreeWidget     *_TWG_Fichiers;
    QTextEdit        *_TXE_Info;
};  
    
#endif
