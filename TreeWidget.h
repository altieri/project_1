//######
//## TreeWidget.h
//## ============
//## 18.06.2018: Ré-écriture complète
//######

#ifndef MYTREEWIDGET_H
#define MYTREEWIDGET_H

#include <QtCore>
#include <QtWidgets>
#include <QDialog>


class myTreeWidget:public QTreeWidget
{
  Q_OBJECT

public:
  myTreeWidget( QWidget* p_Parent );
  ~myTreeWidget();
  
  void  Add_FirstChild( QString p_FilePath );
  void  Add_Children  ( QString p_FilePath, QTreeWidgetItem * p_Item );
  
  void  Adjust_ColumnSize();
  
  QString  Get_PathName( QTreeWidgetItem * p_Item );

  void  Calculer_Checksum( QString p_FilePath );
  
  void  keyPressEvent  ( QKeyEvent   * p_Event );
  void  mousePressEvent( QMouseEvent * p_Event );

  void  contextMenuEvent( QContextMenuEvent * p_Event );

public slots:

  void slot_itemClicked( QTreeWidgetItem * p_Item, int /*p_Column*/ );
  void slot_Calculer_Checksum();
  
};

#endif

