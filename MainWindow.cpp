//######
//## Mainwindow.cpp
//## ==============
//## 18.06.2018: Ré-écriture complète
//######

#include  "MainWindow.h"
#include  "TreeWidget.h"

#include  "FileOpen.xpm"
#include  "FileSave.xpm"
#include "onglet.xpm"

//
// <<<< myMainWindow::myMainWindow
//
myMainWindow::myMainWindow( QWidget * p_Parent )
  : QMainWindow( p_Parent )
{

  //-- MENU

  QMenu * Menu_Fichier = new QMenu( this );
  Menu_Fichier->setTitle( "&Fichier" );

  this->menuBar()->addMenu( Menu_Fichier );

  Menu_Fichier->addAction( QPixmap( ( const char ** ) FileOpen )
    , "Ouvrir Dossier"
    ,  this, SLOT ( slot_Ouvrir_Dossier() ), Qt::CTRL + Qt::Key_O  );

  Menu_Fichier->addAction( QPixmap( ( const char ** ) FileSave )
    , "Sauvegarder Cartographie"
    ,  this, SLOT ( slot_Sauver_Cartographie() ), Qt::CTRL + Qt::Key_S );




QMovie *movie=new QMovie(":/ccc/home/cont001/ocre/altieriq/load.gif");
if (!movie->isValid()) 
    {
     // Something went wrong
    }

	//-- Play GIF

	QLabel *label = new QLabel( this );
	label->setGeometry(115,60,128,128);
	label->setMovie(movie);
	movie->start();
	this, SLOT( slot_Sauver_Cartographie() ), "En cours...";
	label->show();
}



  Menu_Fichier->addSeparator();

  Menu_Fichier->addAction( "Quitter", this , SLOT ( slot_Quitter() ), Qt::CTRL + Qt::Key_Q  );


Menu_Fichier->addAction( QPixmap( ( const char ** ) Add_children )
    , "Onglet"
    ,  this, SLOT ( Add_Children( FilePath, p_Item ) ), Qt::CTRL + Qt::Key_N  );
    

  QMenu * Menu_APropos = new QMenu( this );
  Menu_APropos->setTitle( "?" );

  this->menuBar()->addMenu( Menu_APropos );

  Menu_APropos->addAction( QPixmap( ( const char ** ) FileOpen )
    , "A Propos"
    , this, SLOT ( slot_APropos() ) );

  
  //-- SPLITTER

  _Splitter = new QSplitter( this );
  
  //-- TREEWIDGET Source et TABWIDGET

  _TWG_Fichiers = new myTreeWidget( _Splitter );

  _TXE_Info = new QTextEdit( _Splitter ); 

  QString DefautDIR = QDir::currentPath();

  this->Ouvrir_Dossier( DefautDIR ); 

  int BX = 150;

  QDesktopWidget * Desktop = QApplication::desktop();

  int WW = Desktop->screenGeometry ().width ()  - 2*BX;
  int HH = Desktop->screenGeometry ().height () - 2*BX;

  QRect mRect( BX, BX, WW, HH );

  this->setGeometry( mRect );

  _TXE_Info->setMinimumWidth( WW / 2 );
 
  this->setCentralWidget( _Splitter );
  

  //-- SLOTS

  connect( _TWG_Fichiers, SIGNAL( itemClicked        ( QTreeWidgetItem *, int ) )
         , this         , SLOT  ( slot_Statut_Fichier( QTreeWidgetItem *, int ) ) );
}
// >>>> myMainWindow::myMainWindow


//
// <<<< myMainWindow::Box_Information
//
void myMainWindow::Box_Information( const QString &p_Titre, const QString &p_Msg )
{
  QString Caption = QString( " " )  + p_Titre;
  QString Message = p_Msg + "\n";

  QMessageBox MsgBox;

  //-- https://qt.developpez.com/doc/4.7/qmessagebox/

  MsgBox.setIcon( QMessageBox::Information );

  MsgBox.setWindowTitle( Caption );
  MsgBox.setText( Message );
  MsgBox.setStandardButtons( QMessageBox::Ok );
  MsgBox.setDefaultButton( QMessageBox::Ok );

  (void) MsgBox.exec();
}
// >>>> myMainWindow::Box_Information


//
// <<<< myMainWindow::Ouvrir_Dossier
//
void myMainWindow::Ouvrir_Dossier( QString p_PathDemande )
{
   QFileInfo FI_Srce( p_PathDemande );

   if ( FI_Srce.isSymLink() ) {
    _Path_Dossier = FI_Srce.symLinkTarget();
   } else {
    _Path_Dossier = p_PathDemande;
   }

  _TWG_Fichiers->Add_FirstChild( _Path_Dossier );

  this->Statut_Fichier( "Ouverture Dossier : " + _Path_Dossier );
}
// >>>> myMainWindow::Ouvrir_Dossier


//
// <<<< myMainWindow::Sauver_Cartographie
//
void myMainWindow::Sauver_Cartographie( QString p_PathDemande )
{
  Q_UNUSED( p_PathDemande)

  this->Box_Information( "Sauver_Cartographie>", "En cours..." );
}
// >>>> myMainWindow::Sauver_Cartographie


//
// <<<< myMainWindow::Statut_Fichier
//
void myMainWindow::Statut_Fichier( QString p_Path )
{
  QString PathName = p_Path;

  QFileInfo qFI( PathName );

  if ( qFI.isFile() ) {
    QDate mDate = qFI.lastModified().date();
    QTime mTime = qFI.lastModified().time();

    QString qDate = mDate.toString( "dd.MM.yyyy" );
    QString qTime = mTime.toString( "hh.mm.ss" );

    QString qS_Modif = QString( "  -  Modifié le  %1  %2" ).arg( qDate ).arg( qTime );

    this->statusBar()->showMessage ( PathName + qS_Modif );
  } else {
    this->statusBar()->showMessage ( PathName );
  }
}
// >>>> myMainWindow::Statut_Fichier


//
// <<<< myMainWindow::slot_Ouvrir_Dossier
//

void myMainWindow::slot_Ouvrir_Dossier()
{
  this->Ouvrir_Dossier( "" );
}
// >>>> myMainWindow::slot_Ouvrir_Dossier


//
// <<<< myMainWindow::slot_Sauver_Cartographie
//
void myMainWindow::slot_Sauver_Cartographie()
{
  this->Sauver_Cartographie( "" );
}
// >>>> myMainWindow::slot_Sauver_Cartographie


//
// <<<< myMainWindow::slot_Quitter
//
void myMainWindow::slot_Quitter()
{
  QMessageBox MsgBox;

  MsgBox.setIcon( QMessageBox::Warning );

  MsgBox.setWindowTitle( "Quitter>" );
  MsgBox.setText( "Voulez-vous vraiment quitter l'application ?" );
  MsgBox.setStandardButtons( QMessageBox::Ok | QMessageBox::Cancel );
  MsgBox.setDefaultButton( QMessageBox::Ok );

  int KR = MsgBox.exec();

  if ( KR == QMessageBox::Ok ) {
    qApp->quit();
  }
}
// >>>> myMainWindow::slot_Quitter


//
// <<<< myMainWindow::slot_APropos
//
void myMainWindow::slot_APropos()
{
  QMessageBox MsgBox;

  MsgBox.setIcon( QMessageBox::Warning );

  MsgBox.setWindowTitle( "A Propos>" );
  MsgBox.setText( "Première version...." );
  MsgBox.setStandardButtons( QMessageBox::Ok );
  MsgBox.setDefaultButton( QMessageBox::Ok );

  (void) MsgBox.exec();
}
// >>>> myMainWindow::slot_APropos


//
// <<<< myMainWindow::slot_Statut_Fichier
//
void myMainWindow::slot_Statut_Fichier(  QTreeWidgetItem *p_Item, int p_Column  )
{
  Q_UNUSED( p_Column )

  QString PathName = _TWG_Fichiers->Get_PathName( p_Item );

  this->Statut_Fichier( PathName );
}
// >>>> myMainWindow::slot_Statut_Fichier




