//######
//## TreeAppli.cpp
//## =============
//## 11.04.2018: Création avec include pour Qt5
//## 10.05.2018: Ouvrir_Demande avec lecture TextStream UTF-8
//######

#include "MainWindow.h"
#include "TreeWidget.h"

//
// <<<< main
//
int main( int argc, char **argv )
{
  QApplication app (argc, argv );

  myMainWindow * Window = new myMainWindow();

  Window->setWindowTitle( QString::fromUtf8( "Suivi de Demande de Travaux" ) );

  Window->show();

  return app.exec();
}
// >>>> main
